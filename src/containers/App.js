import React, { Component } from 'react';
import CardList from '../components/CardList/CardList';
import Search from '../components/Search/Search';
import './App.css';

class App extends Component {
  state = {
    robots: [],
    searchField: ''
  };
  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(users => { this.setState({ robots: users })});
  }
  onSearchChange = event => {
    this.setState({ searchField: event.target.value });
  }
  render() {
    const { robots, searchField } = this.state;
    const filteredRobots = robots.filter(robot => {
      return robot.name.toLowerCase().includes(searchField.toLowerCase());
    })
    return (
      <div className="tc">
        <h1 className="f1">Monsters Rolodex</h1>
        <Search searchChange={this.onSearchChange}/>
        <CardList robots={filteredRobots}/>
      </div>
    );
  }
}

export default App;